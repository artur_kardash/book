<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model 
{

    public function create ($user_data)
    {
        return $this->db->insert('users', $user_data);
    }
  
    public function is_registered($login_data)
    {    
        $result=$this->db->get_where('users', array('email'=>$login_data['email'], 'password'=>$login_data['password']));   
        if($result->num_rows()>0)
        {
            return true;
        }else{
            return false;
        }
    }
 
	public function add_books($data) 
    {
        $query = $this->db->insert('books', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function get_books() 
    {
        $this->db->select('books.id, books.name, books.date_create, books.date_update, books.preview, books.date, authors.firstname, authors.lastname');
        $this->db->from('books');
        $this->db->join('authors', 'books.author_id = authors.id');
        $this->db->order_by("UPPER(name)","DESC");
        $query = $this->db->get();
          if ($query)
          {
              return $query->result_array();
          }
          return false;
    }

    public function delete ($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('books');
    }

    public function get_book($id) 
    {
        $this->db->select('*');  
        $this->db->join('authors', 'books.author_id = authors.id');  
        $query = $this->db->get_where('books', array("books.id"=>$id));
        if ($query) 
        {
            return $query->row_array();
        }
          return false;
    }

    public function update($id, $data)
    {
      	$this->db->where('id', $id);
        $this->db->update('books', $data);
    }
      
}