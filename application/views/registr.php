<p><h2 style="color: #b7acac;padding:20px"> Регистрация</h2></p>
<form class="form-horizontal" action = "/main/registr_db" method = "post">
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #9e3ea3" for="lastName">Фамилия *:</label>
    <div class="col-xs-9">
      <input type="text" name= "users[lastname]" style="width:200px" class="form-control" id="lastName" placeholder="Введите фамилию" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #9e3ea3" for="firstName">Имя *:</label>
    <div class="col-xs-9">
      <input type="text" name= "users[firstname]" style="width:200px" class="form-control" id="firstName" placeholder="Введите имя" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #9e3ea3" for="fatherName">Отчество *:</label>
    <div class="col-xs-9">
      <input type="text" name= "users[fathername]" style="width:200px" class="form-control" id="fatherName" placeholder="Введите отчество" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label  style="color: #9e3ea3" class="control-label col-xs-3">Дата рождения:</label>
    <div style="width:120px"class="col-xs-3">
      <select class="form-control" >
       <option style="pointer-events: none;">Число</option>
        <option value='1'> 1 </option>
        <option value='2'> 2 </option>
        <option value='3'> 3 </option>
        <option value='4'> 4 </option>
        <option value='5'> 5 </option>
        <option value='6'> 6 </option>
        <option value='7'> 7 </option>
        <option value='8'> 8 </option>
        <option value='9'> 9 </option>
        <option value='10'> 10 </option>
        <option value='11'> 11 </option>
        <option value='12'> 12 </option>
        <option value='13'> 13 </option>
        <option value='14'> 14 </option>
        <option value='15'> 15 </option>
        <option value='16'> 16 </option>
        <option value='17'> 17 </option>
        <option value='18'> 18 </option>
        <option value='19'> 19 </option>
        <option value='20'> 20 </option>
        <option value='21'> 21 </option>
        <option value='22'> 22 </option>
        <option value='23'> 23 </option>
        <option value='24'> 24 </option>
        <option value='25'> 25 </option>
        <option value='26'> 26 </option>
        <option value='27'> 27 </option>
        <option value='28'> 28 </option>
        <option value='29'> 29 </option>
        <option value='30'> 20 </option>
        <option value='31'> 31 </option>
      </select>
    </div>
    <div style="width:200px"class="col-xs-3">  
      <select name="data" class="form-control">
        <option>Месяц</option>
        <option value='January'> Январь </option>
        <option value='February'> Февраль </option>
        <option value='March'> Март </option>
        <option value='April'> Апрель </option>
        <option value='May'> Май </option>
        <option value='June'> Июнь </option>
        <option value='July'> Июль </option>
        <option value='August'> Август </option>
        <option value='September'> Сентябрь </option>
        <option value='October'> Октябрь </option>
        <option value='November'> Ноябрь </option>
        <option value='December'> Декабрь </option>
      </select>
    </div>
    <div style="width:150px"class="col-xs-3">
      <select class="form-control">
            <option>Год</option>
            <option value='1980'> 1980 </option>
            <option value='1981'> 1981 </option>
            <option value='1982'> 1982 </option>
            <option value='1983'> 1983 </option>
            <option value='1984'> 1984 </option>
            <option value='1985'> 1985 </option>
            <option value='1986'> 1986 </option>
            <option value='1987'> 1987 </option>
            <option value='1988'> 1988 </option>
            <option value='1989'> 1989 </option>
            <option value='1990'> 1990 </option>
            <option value='1991'> 1991 </option>
            <option value='1992'> 1992 </option>
            <option value='1993'> 1993 </option>
            <option value='1994'> 1994 </option>
            <option value='1995'> 1995 </option>
            <option value='1996'> 1996 </option>
            <option value='1997'> 1997 </option>
            <option value='1998'> 1998 </option>
            <option value='1999'> 1999 </option>
            <option value='2000'> 2000 </option>
            <option value='2001'> 2001 </option>
            <option value='2002'> 2002 </option>
            <option value='2003'> 2003 </option>
            <option value='2004'> 2004 </option>
            <option value='2005'> 2005 </option>
    </select>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3"style="color: #9e3ea3" for="inputEmail">Email *:</label>
    <div class="col-xs-9">
      <input type="email" name= "users[email]" style="width:200px"class="form-control" id="inputEmail" placeholder="Email" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #9e3ea3"for="inputPassword">Пароль *:</label>
    <div class="col-xs-9">
      <input type="password" name= "users[password]" maxlength = 16 style="width:200px"class="form-control" id="inputPassword" placeholder="Введите пароль" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #9e3ea3"for="confirmPassword">Подтвердите пароль *:</label>
    <div class="col-xs-9">
      <input type="password" maxlength = 16 style="width:200px" class="form-control" id="confirmPassword" placeholder="Введите пароль ещё раз" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3"style="color: #9e3ea3" for="phoneNumber">Телефон:</label>
    <div class="col-xs-9">
      <input type="tel" style="width:200px"class="form-control" id="phoneNumber" placeholder="Введите номер телефона">
    </div>
  </div>
  <br />
  <div class="form-group">
    <div class="col-xs-offset-3 col-xs-9">
      <input type="submit" class="btn btn-primary" value="Регистрация">
      <input type="reset" class="btn btn-default" value="Очистить форму">
    </div>
  </div>
</form>