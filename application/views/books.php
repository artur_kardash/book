<?php echo ( ! empty($message)? $message:""); ?>
<?php if( ! empty($book)): ?>
  <h3 style="color:#AFEEEE"> <span class="glyphicon glyphicon-info-sign" > </span> Все книги:</h3>
   <table class="table table-hover" border="3" width="50%">
   <thead>
	    <tr> 
	        <th width="5%"> 'ID' </th>
	        <th width="15%"> 'Название книги' </th>
	        <th width="200"> 'Превью' </th>
	        <th width="15%"> 'Автор' </th>
	        <th width="10%"> 'Дата выхода' </th>
	        <th width="15%"> 'Дата добавления' </th>
	        <th width="100"> 'Настройки' </th>
	    </tr>
	</thead>
  	<tbody>
  	 <?php foreach($book as $books): ?>			
	    <tr>
	        <td width="35"><?php echo $books['id']; ?></td>
	        <td width="120"><?php echo $books['name']; ?></td>
	        <td width="20"><a  data-toggle="lightbox" 
	        	href="/assets/<?php echo $books['preview']; ?>" 
	        	class="img-responsive">
	        	 <img src="/assets/<?php echo $books['preview']; ?>" 
	        	 class="img-thumbnail"> 
	        </a></td>
	        <td width="200"> <?php echo $books['firstname'] . " " . $books['lastname'];?> </td>
	        <td width="200"><?php echo $books['date_create']; ?></td>
	        <td width="200"><?php echo $books['date']; ?></td>
	        <td> <a  class="btn btn-success" href="/main/edit/<?php echo $books['id']; ?>">Редактировать</a> 
			 <div class="coll-md-6">
				<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-1"> Просмотр</button>
			 </div>       	
	          <a class="btn btn-success" href="/main/delete/<?php echo $books['id']; ?>"> Удалить </a> </td>
	  </tr> 
	  <?php endforeach; ?>
	</tbody>
    </table>
		<div class="modal fade" id="modal-1">
			<div class="modal-dialog modal-lg">
				<div class="modal-content ">
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal">&times;<button>
							<h4 class="modal-title"> Выбранная книга </h4>
							</div>
							<div class="modal-body">
								<table class="table table-hover" border="3">
									<thead>
										<tr> 
											<th width="15"> 'ID' </th>
											<th width="120"> 'Название книги' </th>
											<th width="200"> 'Превью' </th>
											<th width="200"> 'Автор' </th>
											<th width="200"> 'Дата выхода' </th>
											<th width="200"> 'Дата добавления' </th> 
										</tr>
									</thead>
			  <tbody>
			  	 <?php foreach($book as $books): ?>			
				    <tr>
				        <td width="35"><?php echo $books['id']; ?></td>
				        <td width="120"><?php echo $books['name']; ?></td>
				        <td width="20"><a  data-toggle="lightbox" 
				        	href="/assets/<?php echo $books['preview']; ?>" 
				        	class="img-responsive">
				        	 <img src="/assets/<?php echo $books['preview']; ?>" 
				        	 class="img-thumbnail"> 
				        </a></td>
				        <td width="200"> <?php echo $books['firstname'] . " " . $books['lastname'];?> </td>
				        <td width="200"><?php echo $books['date_create']; ?></td>
				        <td width="200"><?php echo $books['date']; ?></td>
				  </tr> 
				<?php endforeach; ?>
			</tbody>
			  				</table>
							</div>
							<div class="modal-footer">
								<button class="btn btn-danger" type="button" data-dismiss="modal"> Отмена </button>
							</div>
		 		</div>
	 	</div>
</div>	
<?php endif; ?>
