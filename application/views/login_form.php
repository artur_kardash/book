<div id = 'login_form'>
<p><h2 style="color: #b7acac;padding:50px"> Вход</h2></p>
<form class="form-horizontal" style="margin-left:50px" action = "/main/login" method = "post">
  <div class="form-group">
    <label class="control-label col-xs-3"style="color: #9e3ea3" for="inputEmail">Email :</label>
	    <div class="col-xs-9">
	      <input type="email" name= "users[email]" style="width:200px"class="form-control" id="inputEmail" placeholder="Введите email" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
	    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #9e3ea3"for="inputPassword">Пароль :</label>
	    <div class="col-xs-9">
	      <input type="password" name= "users[password]" maxlength = 16 style="width:200px"class="form-control" id="inputPassword" placeholder="Введите пароль" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
	    </div>
  </div>
	 <div class="form-group">
	    <div class="col-xs-offset-3 col-xs-9">
	      <input type="submit" class="btn btn-primary" value="Вход">
	      <input type="reset" class="btn btn-default" value="Очистить форму">
	    </div>
	</div>
</form>
</div>


