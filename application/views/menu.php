<body>
<nav class="navbar navbar-inverse" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
      <a class="navbar-brand" href="/">Books</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Поиск</button>
      </form>
    <?php if($this->session->userdata('is_logged_in')): ?>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/main/logout">Выход</a></li>
            <li><a href="/main/books">Таблица книг</a></li>
          </ul>
    <?php else: ?>
           <ul class="nav navbar-nav navbar-right">
            <li><a href="/main/login_form">Вход</a></li>
            <li><a href="/main/registr">Регистрация</a></li>
          </ul>
     <?php endif; ?>     
    </div>
  </div>
</nav>