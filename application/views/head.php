<!DOCTYPE html>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  	<link href="/style.css" rel="stylesheet">
  	<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="/bootstrap-lightbox/ekko-lightbox.min.css">
  	<script type="text/javascript" src="/2.1.1.min.js"></script>
	<script src="/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/bootstrap-lightbox/ekko-lightbox.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/js_book.js"></script>

  	<title><?php echo $title; ?></title>
  </head>
