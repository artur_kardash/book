<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
	}

	public function index()
	{
		$this->home();

	}

	public function home()
	{
		$data['title'] = "Главная";
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('footer');
	}

	public function registr()
	{
		$data['title'] = "Регистрация";
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('registr');
		$this->load->view('footer');
	}

	public function registr_db()
	{
		$this->load->library("form_validation");               
		$this->form_validation->set_rules('users[email]', 'E-mail', 'trim|required|valid_email');
		$this->form_validation->set_rules('users[firstname]', 'Имя', 'trim|required');
		$this->form_validation->set_rules('users[lastname]', 'Фамилия', 'trim|required');
		$this->form_validation->set_rules('users[fathername]', 'Отчество', 'trim|required');
		$this->form_validation->set_rules('users[password]', 'Пароль', 'trim|required|min_length[7]|md5');         
		if($this->form_validation->run())
		 {
		   $user = $this->input->post('users');
		   $this->load->model('users_model');
   			if ($this->users_model->create($user))
				   {
				     $this->session->set_userdata(array('is_logged_in'=>true));
				     $data['title'] = 'Список книг';
				     $this->load->view('head', $data);
				     $this->load->view('menu');
				     $this->load->view('books');
				     $this->load->view('footer');
				     }else {	 
						    $data['title'] = 'Нет доступа';
						    $this->load->view('head', $data);
						    $this->load->view('menu');
						    $this->load->view('footer');              
		            		}
		 }
	}

	 public function logout()
     {
          $this->session->sess_destroy();
          header("Location: /");
     }

 	 public function login_form()
     {
		 $data['title'] = 'Вход';
		 $this->load->view('head', $data);
		 $this->load->view('menu');
		 $this->load->view('login_form');
		 $this->load->view('footer');
      }

  	 public function login()
     { 
      	$this->load->library('form_validation');
		$this->form_validation->set_rules('users[email]', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('users[password]', 'Password', 'required|md5|trim|min_length[3]');
        if($this->form_validation->run())
    	{
          $user=$this->input->post('users');
          $this->load->model('users_model');
        	if($this->users_model->is_registered($user))
        	{
            $this->session->set_userdata(array('is_logged_in'=>true));
            header("Location: /main/books");
            die();
        	}
     	}
		 $data['title'] = 'Неправельный логин либо пароль!';
		 $this->load->view('head', $data);
		 $this->load->view('menu');
		 $this->load->view('login_form');
		 $this->load->view('footer');
	 } 

	 public function books()
	 {
		 $data['title'] = 'Таблица книг';
		 $book_data['book'] = $this->users_model->get_books();
		 $this->load->view('head', $data);
		 $this->load->view('menu');
		 $this->load->view('books', $book_data);
		 $this->load->view('footer');
	 }

	public function book_db()
	{
		$this->load->library("form_validation");
		$this->form_validation->set_rules('books[name]', 'required');
		$this->form_validation->set_rules('books[date_create]', 'required');
		$this->form_validation->set_rules('books[preview]', 'Комментарий', 'required');
		$this->form_validation->set_rules('author[firstname]', 'required');
		$this->form_validation->set_rules('author[lastname]', 'required');
		if($this->form_validation->run())
		{
		$data = $this->input->post('books', 'author');
		$data['date'] = date("Y-m-d H:i:s");				      
		$this->users_model->add_books($data);
		}
	}
	
	public function delete($id)
	{		
		$this->users_model->delete($id);
		header("Location: /main/books");
	}
	
	public function edit($id)
	{
		$data['title'] = 'Редактирование';
		$book_data['book'] = $this->users_model->get_book($id);
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('edit', $book_data);
		$this->load->view('footer');
	}
	
	public function update()
	{		
		$this->load->model('users_model');	
		$data=$this->input->post('book');
		$id=$this->input->post('id');
		$data['date_update'] = date("Y-m-d H:i:s");
		$this->users_model->update($id, $data);
		$this->books();
	}
		
}





